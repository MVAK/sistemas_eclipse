package Sesion1;

import java.util.Arrays;
import java.util.Scanner;

public class prueba6 {

	
	
	public static void main(String[] args) {
	
		Scanner leerTeclado= new Scanner(System.in);
		System.out.println("Bienvenido a juegos DAM");
		System.out.println("Las opciones son: \n 1 -> Dibujar figuras geometricas \n 2-> Ordenar palabras \n 3->  Salir \n");
		int opcion= leerTeclado.nextInt();
		boolean exit = false;// para luego compararlo y terminar el programa
		
		do {
			switch(opcion) {
			   case 1:
			      menu1();// donde estaran las figuras geometricas
			      break;
			   case 2:
			      ordenarpalabras();// donde hara lo de las vocales y consonantes
			      break;
			   case 3:
			      System.out.println("Gracias por jugar jeje Hasta la proxima");
			      exit = true;//te vas a terminar el codigo
			      break;
			   default:
				   System.out.println("Recuerde: las opciones son : \n 1 -> dibujar \n 2 ->jugar palabra \n 3 ->salir\n");
			}
			if(!exit) {// si no sale significa que quiere seguir jugando
				System.out.println("Desea seguir jugando? ya sabes son:\n 1-> dibujar \n 2-> jugar palabra \n 3-> salir\n");
				opcion = leerTeclado.nextInt();
			}
		}while(exit==false);
		leerTeclado.close();

	}
	
	
	private static void ordenarpalabras() {// la funcion de las palabras
		Scanner leerTeclado= new Scanner(System.in);
		int aux=0;
		String palabrapeticion;
		System.out.print("Escribe lo que quieres que separe y ordene \n");
		palabrapeticion= leerTeclado.next();// guardamos todo lo que quiera meter con mayus y minus
		char todo[]= new char[palabrapeticion.length()];
		char vocales[]= new char[palabrapeticion.length()];
		char consonantes[]= new char[palabrapeticion.length()];
		for(int i=0;i<palabrapeticion.length()-1;i++)
			todo[i]= palabrapeticion.charAt(i);
		for(int i = 0; i < palabrapeticion.length(); i++)
			todo[i]= Character.toLowerCase(todo[i]);//transformamos toda la frase o palabras en minuscula para evitar errores
		for(int k=0;k < palabrapeticion.length(); k++) {
			if(todo[k]=='a'||todo[k]=='e'||todo[k]=='i'||todo[k]=='o'||todo[k]=='u') {//separamos las vocales
				vocales[aux]= todo[k];//ordenamos vocales
				Arrays.sort(vocales);
			}
				else consonantes[aux]=todo[k];// de las consonantes
			Arrays.sort(consonantes);//ordenamos consonantes
		}
		for(int i=0; i < vocales.length ; i++)//mostrar todas las vocales ordenadas
		System.out.print(vocales[i]);
		for(int i=0; i < consonantes.length ; i++)//mostrar las consonentes ordenadas
			System.out.print(consonantes[i]);
		leerTeclado.close();
	}
	
	private static void menu1() {//el menu de la figuras geometricas
		Scanner leerTeclado= new Scanner(System.in);
		System.out.println("Las opciones que puedes dibujar son:\n 1-> cuadrado\n-> 2 triangulo\n -> 3 rombo\n 4-> salir");
		boolean menu1= false;
		int eleccion = leerTeclado.nextInt();
		do {
			switch(eleccion) {
			case 1:
				cuadrado();
				break;
			case 2:
				triangulorect();
				break;
			case 3:
				rombo();
				break;
			case 4:
				System.out.println("Volviendo al menu principal\n");
				menu1 = true;
				break;
			default:
				System.out.println("Las opciones que puedes dibujar son: 1 cuadrado, 2 triangulo, 3 rombo, 4 salir");
				eleccion = leerTeclado.nextInt();
			}
			if (!menu1) {//si no elige ninguna de las opciones te pregunta si quieres seguir jugando
				System.out.println("Quieres seguir jugando?\n");
				eleccion= leerTeclado.nextInt();
				
			}
		}while (menu1 == false);
		leerTeclado.close();
	}

	private static void cuadrado() {
		Scanner leerTeclado= new Scanner(System.in);
		int lado;
		System.out.println("Valor del lado: \n");
		lado = leerTeclado.nextInt();// recojo el lado del cuadrado ya que como tiene los lados iguales solo hay que recoger uno
		if (lado >1) {//para poder entrar en el cuadrado
			for(int i = 0; i < lado; i++) {//para hacer piso a piso
				for (int j = 0; j < lado ; j++) {//para hacer la base de cada piso
					if (i==lado - 1 || i == 0 || j == 0 || j == lado - 1)
						System.out.print("*");
					else 
						System.out.print(" ");
				}
				System.out.println();
			}
		}else 
			System.out.println ("El primer lado debe ser impar y mayor que 0 para que pueda funcionar");
		leerTeclado.close();
				}
			
		
		private static void triangulorect() {
			Scanner leerTeclado= new Scanner(System.in);
			int base;
			System.out.println("Valor de la base de triangulo rectangulo: \n");
			base = leerTeclado.nextInt();
			 if(base>2) {
				for(int i = 1; i <=base ; i++) {//para decir que fila es y avanzar
					for(int j = 1; j <= i ; j++) {//poner espacios huecos
							if(i==j || i==1 || j==1|| i== base) {//la condicion para que escriba asterisco
								System.out.print("*");
							}else System.out.print(" ");
					}
					System.out.println();//salto de linea
					}
					
					
				
		
			}else 
				System.out.println("La base de la piramide tiene que ser mayor que 0 e impar \n");
			 leerTeclado.close();
		}
		private static void rombo() {//la parte de arriba es lo mismo que la piramide de la practica anterior
			Scanner leerTeclado= new Scanner(System.in);
			int base;
			System.out.println("Valor de la base del rombo: \n");
			base = leerTeclado.nextInt();
			int k = 2*base -2;//lo unico que aqui lo ajustamos para la operacion que hacemos dentro
			if (base %2 == 1 && base >= 0) {
				for (int i=0; i<base; i++)// es dibujar la piramide y hacer su inversa
			    {
			        for (int j=0; j<k; j++)
			        	System.out.print(" ");
			        k = k - 1;
			 
			        for (int j=0; j<=i; j++ )
			        {
			        	System.out.print(" *");
			        }
			        System.out.println();
			    }

			    for (int i=base; i>=0; i--)// aqui hacemos la inversa de la piramide
			    {
			        for (int j=k; j>0; j--)
			        	System.out.print(" ");
			        k = k + 1;
			 
			        for (int j=i; j>=0; j-- )
			        {
			        	System.out.print(" *");
			        }
			        System.out.println();
			    }
			}
			else 
				System.out.println("La base del rombo tiene que ser mayor que 0 e impar \n");
		
		
			leerTeclado.close();	
		}
}
		
		
				
	
		
		
	



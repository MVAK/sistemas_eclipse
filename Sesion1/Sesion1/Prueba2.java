package Sesion1; 

public class Prueba2 {
	
	public static void main(String[] args) {
	String fragmentoNombreAlumnos = "Victor";
	String[] nombreAlumnos = {"Antonio","Marta","Victor Hugo","David"};
	
	int resultado = contarUsuarios(fragmentoNombreAlumnos, nombreAlumnos);
	System.out.println("Total resultados: " + resultado);
}
	
	static int contarUsuarios(String fragmentoNombreAlumno, String[] nombreAlumnos) {
		boolean encontrado = false;
		int totalEncontrados = 0;
		for (String nombreAlumnoActual : nombreAlumnos) {
			if (nombreAlumnoActual.contains(fragmentoNombreAlumno)) {
				encontrado = true;
			}
			if (encontrado) {
				totalEncontrados++;
				encontrado = false;
			}
		}
		return totalEncontrados;
	}
}

// lo unico que fallaba era que le faltaba poner el encontrado = false,
//porque sino por predeterminado lo daba por true y se sumaba
// uno mas por lo que el total resultados daba 2
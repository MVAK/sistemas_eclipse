
import java.util.Scanner;


public class Ejercicio_ec_2grado {


	public static void main(String[] args) {
		
		double numero, grad1, grad2, sol1 = 0, sol2 = 0; // el numero no tiene incognita, grad1 es la incognita de grado uno
		// grad2 es la incognita al cuadrado
		Scanner leerTeclado = new Scanner(System.in);//recoger los datos del usuario
		System.out.println("Introduce el numero que va a ir sin incognita");
		numero= leerTeclado.nextDouble();
		System.out.println("Introduce el numero que va a ir acompa�ado de una x que no este elevado a nada");
		grad1= leerTeclado.nextDouble();
		System.out.println("Introduce el numero que va a ir acompa�ado de una x que este elevado al cuadrado");
		grad2= leerTeclado.nextDouble();
		if((grad1*grad1)-4*numero*grad2<0) // para comprobar si la raiz es negativa, si lo es no tiene solucion
			System.out.println("No tiene soluci�n ");
		else { // si la raiz es positiva significa que tiene solucion
		sol1 = (-(grad1)+(Math.sqrt((grad1*grad1)-4*numero*grad2)))/(2*numero);// esta solucion es la que se suma el resultado de la raiz 
		sol2 = (-(grad1)-(Math.sqrt((grad1*grad1)-4*numero*grad2)))/(2*numero);// y esta el que se resta
		
		System.out.println("La primera solucion de esta ecuacion es: "+sol1);// printamos la solucion 1 que es la que se suma la raiz
		System.out.println("La segunda solucion de esta ecuacion es: "+sol2);// printamos la solucion 1 que es la que se resta la raiz
		
		}
		leerTeclado.close();
		
		

	}

}
